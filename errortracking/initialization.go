package errortracking

import (
	"github.com/getsentry/sentry-go"
)

// Initialize will initialize error reporting
func Initialize(opts ...InitializationOption) error {
	config := applyInitializationOptions(opts)

	sentryClientOptions := sentry.ClientOptions{
		Dsn:         config.sentryDSN,
		Environment: config.sentryEnvironment,
	}

	if config.version != "" {
		sentryClientOptions.Release = config.version
	}

	return sentry.Init(sentryClientOptions)
}
