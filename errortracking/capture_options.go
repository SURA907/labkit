package errortracking

import (
	"github.com/getsentry/sentry-go"
)

// CaptureOption will configure how an error is captured
type CaptureOption func(*sentry.Event)
