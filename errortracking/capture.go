package errortracking

import (
	"reflect"

	"github.com/getsentry/sentry-go"
)

// Capture will report an error to the error reporting service
func Capture(err error, opts ...CaptureOption) {
	event := sentry.NewEvent()
	event.Level = sentry.LevelError

	for _, v := range opts {
		v(event)
	}

	event.Exception = []sentry.Exception{
		{
			Type:       reflect.TypeOf(err).String(),
			Value:      err.Error(),
			Stacktrace: sentry.ExtractStacktrace(err),
		},
	}

	sentry.CaptureEvent(event)
}
