package errortracking_test

import (
	"net/http"

	"gitlab.com/gitlab-org/labkit/errortracking"
)

func ExampleCapture() {
	req, err := http.NewRequest("GET", "http://example.com", nil)
	ctx := req.Context()

	if err != nil {
		// Send the error to the error tracking system
		errortracking.Capture(err,
			errortracking.WithContext(ctx),                          // Extract details such as correlation-id from context
			errortracking.WithRequest(req),                          // Extract additional details from request
			errortracking.WithField("domain", "http://example.com")) // Add additional custom details
	}
}

func ExampleNewHandler() {
	handler := http.HandlerFunc(func(http.ResponseWriter, *http.Request) {
		panic("oh dear")
	})

	http.ListenAndServe(":1234", errortracking.NewHandler(handler))
}
