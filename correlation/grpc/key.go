package grpccorrelation

const (
	metadataCorrelatorKey = "X-GitLab-Correlation-ID"
	metadataClientNameKey = "X-GitLab-Client-Name"
)
